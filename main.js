const student = {
  name: "",
  lastName: "",
  grades: {},
  };
  
  // запит імені та прізвища студента
  student.name = prompt("Введіть ім'я студента:");
  student.lastName = prompt("Введіть прізвище студента:");
  
  // запит назв та оцінок з предметів
  let subject = "";
  let grade = "";
  while (subject !== null) {
  subject = prompt("Введіть назву предмету:");
  if (subject !== null) {
  grade = prompt("Введіть оцінку для предмету"+ " "+[subject]);
  student.grades[subject] = parseInt(grade);
  }
  }
  
  // порахувати кількість поганих оцінок
  let badGrades = 0;
  for (let subject in student.grades) {
  if (student.grades[subject] < 4) {
  badGrades++;
  }
  }
  
  // вивести результат
  const totalSubjects = Object.keys(student.grades).length;
  const averageGrade =
  totalSubjects > 0
  ? Object.values(student.grades).reduce((a, b) => a + b) / totalSubjects
  : 0;
  
  if (badGrades > 0) {
  alert('Студент отримав'+" " +[badGrades]+" "+ 'поганих оцінок');
  } else {
  alert("Студент переведено на наступний курс");
  }
  
  if (averageGrade > 7) {
  alert("Студенту призначено стипендію");
  }